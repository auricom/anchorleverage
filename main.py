import os, traceback, datetime
from time import sleep

from src.functions import logger
from src.terra_functions import Terra
from src.anchor import (
    get_anchor_borrow_limit,
    get_aust_value,
    get_anchor_all_collateral_value,
    get_anchor_borrow_usage,
    anchor_earn_withdraw,
    get_anchor_borrow_amount,
    anchor_borrow_borrow,
    anchor_borrow_repay,
    anchor_deposit_bluna_collateral,
    get_anc_pending_value,
    anchor_claim_anc_rewards,
    get_anc_value,
    get_earn_to_borrow_ratio,
    get_anchor_collateral_amounts,
    get_anchor_collateral_value,
    get_anchor_asset_borrow_limit,
    anchor_withdrawal_bluna_collateral,
    get_anchor_oracle_prices,
)
from src.swap import (
    leverage_up,
    swap_anc_to_ust,
    get_leverage_amounts,
    swap_luna_to_ust,
    swap_bluna_to_ust,
)
from src.telegram import setup_bot


# Function: When ust balance is less than min ust balance, withdraw ust from anchor earn to reach maximum ust balance
def ust_below_min_ust_balance(terra_instance, balances):
    amount_to_withdrawal = terra_instance.wallet_max_ust_balance - balances["usd"]
    logger(f"Withdrawing {amount_to_withdrawal} from Anchor Earn", log_type=0)

    if amount_to_withdrawal > aust_balance:
        raise Exception(
            f"aUST balance ${aust_balance} is less than amount to withdrawal ${amount_to_withdrawal} to bring you above the minimum balance of ${terra_instance.wallet_min_ust_balance}"
        )

    txhash = anchor_earn_withdraw(terra_instance, amount_to_withdrawal)

    logger(f"Withdrawed UST from Anchor Earn TXHASH: {txhash}", log_type=0)


# Function: When ust balance is above max ust balance, deposit percentage into anchor earn swap the rest for bluna for more collateral
def ust_above_max_ust_balance(terra_instance, balances):
    amount_to_deposit_total = balances["usd"] - terra_instance.wallet_max_ust_balance

    borrowed_amount = get_anchor_borrow_amount(terra_instance)
    earn_amount = get_aust_value(terra_instance)
    collateral_amount = get_anchor_all_collateral_value(terra_instance)

    target_earn_amount = (
        borrowed_amount * terra_instance.ust_earn_to_borrow_target_ratio
    )

    logger(
        f"Amount to deposit total: {amount_to_deposit_total} Borrowed_amount: {borrowed_amount} Earn_amount: {earn_amount} Target Earn Amount: {target_earn_amount}",
        log_type=1,
    )

    if (
        target_earn_amount - (target_earn_amount * 0.01)
        <= earn_amount
        <= target_earn_amount + (target_earn_amount * 0.01)
    ) or (borrowed_amount < 1 and earn_amount < 1):
        # If earn amount within 1% of leverage amount or if this is a new wallet
        logger(f"Earn withing 1% of target, Splitting like normal", log_type=1)
        use_earn_amount, use_luna_amount = get_leverage_amounts(
            terra_instance, amount_to_deposit_total
        )

    elif earn_amount < target_earn_amount:
        # Not enough in earn compared to leverage target
        logger(f"Not enough in earn to leverage, Focusing on earn", log_type=1)
        earn_amount_needed = target_earn_amount - earn_amount
        logger(f"Earn amount needed: {earn_amount_needed}", log_type=1)
        if earn_amount_needed > amount_to_deposit_total:
            # Not enough UST to reach leverage target
            use_earn_amount = amount_to_deposit_total
            use_luna_amount = 0
        else:
            # Leftover UST can be used to leverage
            use_earn_amount = earn_amount_needed
            leftover = amount_to_deposit_total - use_earn_amount

            tmp_earn_amount, use_luna_amount = get_leverage_amounts(
                terra_instance, leftover
            )
            use_earn_amount += tmp_earn_amount

    else:
        # If more in earn than leverage target
        logger(f"More than enough in earn to leverage, Focusing on luna", log_type=1)

        collateral_needed = (
            earn_amount * terra_instance.leverage_swapping_ratio
        ) - collateral_amount
        logger(f"Collateral needed: {collateral_needed}", log_type=1)

        if collateral_needed > amount_to_deposit_total:
            # Not enough UST to reach leverage target
            use_luna_amount = amount_to_deposit_total
            use_earn_amount = 0
        else:
            # Leftover UST can be used to leverage
            use_luna_amount = collateral_needed
            leftover = amount_to_deposit_total - use_luna_amount

            use_earn_amount, tmp_luna_amount = get_leverage_amounts(
                terra_instance, leftover
            )
            use_luna_amount += tmp_luna_amount

    logger(
        f"Purchasing amounts: Earn: {use_earn_amount} | Luna: {use_luna_amount}",
        log_type=0,
    )
    leverage_up(terra_instance, use_earn_amount, use_luna_amount)

    balances = terra_instance.get_account_native_balance()
    aust_balance = get_aust_value(terra_instance)
    bluna_balance = terra_instance.get_token_amount(terra_instance.bLUNA_token)

    logger(f"Native Balances: {balances}", log_type=0)
    logger(f"Anchor Earn Balance: {aust_balance}", log_type=0)
    logger(f"bLuna Balance: {bluna_balance}", log_type=0)


# Function: When usage is less than min usage threshold, borrow more collateral
def borrow_usage_below_min_usage(terra_instance):
    borrow_limit = get_anchor_borrow_limit(terra_instance)
    borrowed_amount = get_anchor_borrow_amount(terra_instance)

    if borrow_limit > 0:
        amount_to_borrow = (
            borrow_limit * terra_instance.anchor_usage_target_percentage
        ) - borrowed_amount

        logger(f"Borrowing {amount_to_borrow}", log_type=0)
        txhash = anchor_borrow_borrow(terra_instance, amount_to_borrow)
        logger(f"Borrowing completed TXHASH: {txhash}", log_type=0)


# Function: When usage is greater than max usage threshold, repay the loan
def borrow_usage_greater_than_max_usage(terra_instance):
    wallet_balance = terra_instance.get_account_native_balance()["usd"]
    borrow_limit = get_anchor_borrow_limit(terra_instance)
    borrowed_amount = get_anchor_borrow_amount(terra_instance)
    amount_to_repay = borrowed_amount - (
        borrow_limit * terra_instance.anchor_usage_target_percentage
    )

    # Take into account wallet balance
    amount_to_withdrawal = amount_to_repay - (
        wallet_balance - terra_instance.wallet_min_ust_balance
    )
    logger(
        f"Wallet Balance: {wallet_balance}\nAmount to repay: {amount_to_repay}\nAmount to withdrawal: {amount_to_withdrawal}",
        log_type=0,
    )

    if amount_to_repay < 0:
        raise Exception(
            f"Amount to repay ${amount_to_repay} is less than 0, this should not happen"
        )

    if amount_to_withdrawal > aust_balance:
        logger(
            f"aUST balance ${aust_balance} is less than amount to withdrawal ${amount_to_withdrawal} needed to repay loan",
            0,
        )
        amount_to_withdrawal = aust_balance

    if amount_to_withdrawal > 1:
        # Withdraw UST from Anchor Earn, minimum of $1 so its actually more than the transaction fee and stops really low withdrawals
        logger(f"Withdrawing ${amount_to_withdrawal} from Anchor Earn", log_type=0)
        txhash = anchor_earn_withdraw(terra_instance, amount_to_withdrawal)
        logger(f"Withdrawed UST from Anchor Earn TXHASH: {txhash}", log_type=0)

    logger("Repaying loan", log_type=0)
    txhash = anchor_borrow_repay(terra_instance, amount_to_repay)

    logger(f"Repaid loan TXHASH: {txhash}", log_type=0)


def anc_pending_greater_than_max_anc(terra_instance):
    txhash = anchor_claim_anc_rewards(terra_instance)
    logger(f"Claimed ANC rewards TXHASH: {txhash}", log_type=0)


def anc_above_max_anc(terra_instance):
    anc_balance = terra_instance.get_token_amount(terra_instance.anc_token)
    # Sell anc for ust
    logger(f"Selling {anc_balance} ANC for ust", log_type=0)
    swap_anc_to_ust(terra_instance, anc_balance)

    balances = terra_instance.get_account_native_balance()
    logger(f"Native Balances: {balances}", log_type=0)


def luna_above_zero(terra_instance, balances):
    logger(f"Selling {balances['luna']} luna for ust", log_type=0)
    swap_luna_to_ust(terra_instance, balances["luna"])


def above_earn_to_borrow_ratio_max(terra_instance, earn_to_borrow_ratio):
    logger(
        f"Earn to borrow ratio {earn_to_borrow_ratio} is greater than max ratio {terra_instance.ust_earn_to_borrow_max_ratio}",
        log_type=0,
    )

    target_earn_to_borrow_amount = (
        get_anchor_borrow_amount(terra_instance)
        * terra_instance.ust_earn_to_borrow_target_ratio
    )
    earn_amount = get_aust_value(terra_instance)
    withdrawal_amount = earn_amount - target_earn_to_borrow_amount

    if withdrawal_amount < 0:
        raise Exception(
            f"Withdrawal amount ${withdrawal_amount} is less than 0, this should not happen"
        )

    logger(f"Withdrawing ${withdrawal_amount} from Anchor Earn", log_type=0)
    txhash = anchor_earn_withdraw(terra_instance, withdrawal_amount)
    logger(
        f"Withdrawed ${withdrawal_amount} from Anchor Earn TXHASH: {txhash}", log_type=0
    )


def below_earn_to_borrow_ratio_min(terra_instance, earn_to_borrow_ratio):
    logger(
        f"Earn to borrow ratio {earn_to_borrow_ratio} is less than min ratio {terra_instance.ust_earn_to_borrow_min_ratio}",
        log_type=0,
    )

    # withdrawal X collateral to bring LTV to 90% and sell for UST
    borrow_amount = get_anchor_borrow_amount(terra_instance)
    borrow_limit = get_anchor_borrow_limit(terra_instance)
    target_borrow_limit_amount = (
        borrow_amount / terra_instance.deleverage_borrow_usage_target
    )
    borrow_reduction = borrow_limit - target_borrow_limit_amount
    bluna_price = get_anchor_oracle_prices(terra_instance)[
                terra_instance.bLUNA_token
            ]
    bluna_borrow_limit_ltv = get_anchor_asset_borrow_limit(
            terra_instance, terra_instance.bLUNA_token
        )


    anchor_collateral_amounts = get_anchor_collateral_amounts(terra_instance)
    if terra_instance.bLUNA_token in anchor_collateral_amounts:
        bluna_collateral_amount = anchor_collateral_amounts[terra_instance.bLUNA_token]
        withdrawal_amount = borrow_reduction / bluna_price / bluna_borrow_limit_ltv

        if withdrawal_amount > bluna_collateral_amount:
            # Not enough bluna collateral to reach 90% LTV
            logger(
                "Not enough bluna collateral to reach 90% LTV",
                log_type=1,
            )
            withdrawal_amount = bluna_collateral_amount

        anchor_withdrawal_bluna_collateral(terra_instance, withdrawal_amount)

        bluna_balance = terra_instance.get_token_amount(terra_instance.bLUNA_token)
        logger(f"Selling {bluna_balance} Bluna for ust", log_type=0)
        swap_bluna_to_ust(terra_instance, bluna_balance)


def loop_main(terra_instance):
    try:
        balances = terra_instance.get_account_native_balance()

        # If UST balance is below min ust balance then withdraw from anchor earn til it reaches the maximum ust balance
        if "usd" in balances:
            if balances["usd"] < terra_instance.wallet_min_ust_balance:
                logger(
                    f"UST balance {balances['usd']} is below minimum balance of {terra_instance.wallet_min_ust_balance}",
                    log_type=0,
                )
                ust_below_min_ust_balance(terra_instance, balances)
                balances = terra_instance.get_account_native_balance()

        if "luna" in balances:
            logger(f"Luna balance {balances['luna']} is above 0", log_type=0)
            luna_above_zero(terra_instance, balances)

        # Get bluna balance
        bluna_balance = terra_instance.get_token_amount(terra_instance.bLUNA_token)
        # If you have bluna, deposit it as collateral to anchor borrow
        if bluna_balance > 0:
            logger(
                f"bLuna balance of {bluna_balance} is being deposited as collateral",
                log_type=0,
            )
            anchor_deposit_bluna_collateral(terra_instance, bluna_balance)

        # Get anchor borrow usage
        borrow_usage = get_anchor_borrow_usage(terra_instance)
        # If usage is below min usage threshold, borrow more to reach target usage
        if borrow_usage < terra_instance.anchor_usage_min_percentage:
            logger(
                f"Usage {borrow_usage*100}% is below min usage threshold, borrowing more to reach {terra_instance.anchor_usage_target_percentage*100}% usage",
                log_type=0,
            )
            borrow_usage_below_min_usage(terra_instance)

        # If usage is above max usage threshold, repay loan to reach target usage
        if borrow_usage > terra_instance.anchor_usage_max_percentage:
            logger(
                f"Usage is currently at {borrow_usage}\nThis is above max usage threshold, repaying loan to reach target usage",
                log_type=0,
            )
            borrow_usage_greater_than_max_usage(terra_instance)

        # Get anc pending value and collect it if it is greater than max anc
        anc_pending_value = get_anc_pending_value(terra_instance)
        if anc_pending_value > terra_instance.anchor_anc_max_balance:
            logger(
                f"ANC pending balance of ${anc_pending_value} greater than max balance ${terra_instance.anchor_anc_max_balance}, collecting it",
                log_type=0,
            )
            anc_pending_greater_than_max_anc(terra_instance)

        # Get anc balance and sell it if it is above max anc
        anc_value = get_anc_value(terra_instance)
        if anc_value > terra_instance.anchor_anc_max_balance:
            logger(
                f"ANC value of ${anc_value} is greater than max ${terra_instance.anchor_anc_max_balance}, selling it for UST",
                log_type=0,
            )
            anc_above_max_anc(terra_instance)

        # If UST balance is above max ust balance then deposit into anchor earn and swap to bluna
        balances = terra_instance.get_account_native_balance()
        if balances["usd"] > terra_instance.wallet_max_ust_balance:
            logger(
                f"UST wallet {balances['usd']} balance greater than max wallet balance ${terra_instance.wallet_max_ust_balance}",
                log_type=0,
            )
            ust_above_max_ust_balance(terra_instance, balances)

        earn_to_borrow_ratio = get_earn_to_borrow_ratio(terra_instance)
        bluna_balance = terra_instance.get_token_amount(terra_instance.bLUNA_token)

        # Only check if there is no bluna balance
        if bluna_balance < 1 or bluna_balance is None:
            if earn_to_borrow_ratio > terra_instance.ust_earn_to_borrow_max_ratio:
                above_earn_to_borrow_ratio_max(terra_instance, earn_to_borrow_ratio)
                earn_to_borrow_ratio = get_earn_to_borrow_ratio(terra_instance)

            if earn_to_borrow_ratio < terra_instance.ust_earn_to_borrow_min_ratio:
                below_earn_to_borrow_ratio_min(terra_instance, earn_to_borrow_ratio)
                earn_to_borrow_ratio = get_earn_to_borrow_ratio(terra_instance)

        # Loop complete
        borrow_usage = get_anchor_borrow_usage(terra_instance)
        if earn_to_borrow_ratio is None:
            earn_to_borrow_ratio_txt = "N/A"
        else:
            earn_to_borrow_ratio_txt = f"{earn_to_borrow_ratio*100:.2f}%"

        logger(
            f"[{datetime.datetime.now()}] Loop Complete, usage currently at {borrow_usage*100}%, Earn to Borrow Ratio {earn_to_borrow_ratio_txt} sleeping for {terra_instance.sleep_duration} seconds",
            log_type=0,
        )

    except Exception as error:
        raise Exception(error)


if __name__ == "__main__":
    try:
        terra_instance = Terra()

        if terra_instance.notify_telegram:
            setup_bot(terra_instance)

        balances = terra_instance.get_account_native_balance()
        aust_balance = get_aust_value(terra_instance)
        aust_amount = terra_instance.get_token_amount(terra_instance.aUST_token)
        borrow_usage = get_anchor_borrow_usage(terra_instance)

        logger(f"Native Balances: {balances}", log_type=0)
        logger(f"Anchor Earn Balance: {aust_balance}", log_type=0)
        logger(f"aUST Amount: {aust_amount}", log_type=1)
        logger(f"Anchor Borrow Usage: {borrow_usage*100}%", log_type=0)

        while True:
            loop_main(terra_instance)
            sleep(terra_instance.sleep_duration)

    except Exception as error:
        if isinstance(error, list):
            for message in error:
                logger(message, log_type=2)
        else:
            logger(error, log_type=2)

        logger("Exiting", log_type=2)

        print(traceback.format_exc())

        os._exit(1)

    except KeyboardInterrupt:
        logger("Exiting", log_type=0)
        os._exit(1)
