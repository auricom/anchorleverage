# AnchorLeverage

## Description

Manage your Anchor Borrow Usage and utilize it to leverage up on additional Luna.

This will help you to increase your luna and increase your ROI. It will allow you to leverage up and loop your luna into additional collateral.

Similar projects that were used as a reference include

-   [AnchorHODL](https://github.com/unl1k3ly/AnchorHODL)
-   [anchor-borrow-bot](https://github.com/foolproof/anchor-borrow-bot)

## Badges

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/ae3d978496f24fe996e331ae223c3958)](https://www.codacy.com/gl/Luigi311/anchorleverage/dashboard?utm_source=gitlab.com&utm_medium=referral&utm_content=Luigi311/anchorleverage&utm_campaign=Badge_Grade)

## Visuals

![AnchorLeverage Process](./Documentation/AnchorLeverage_Process.svg)

## Installation

### Baremetal

-   Setup virtualenv of your choice

-   Install dependencies

    ```bash
      pip install -r requirements.txt
    ```

-   Create a .env file similar to .env.sample and set the MNEMONIC variable to your seed phrase

-   Run

    ```bash
    python main.py
    ```

### Docker

-   Build docker image

    ```bash
    docker build -t anchorleverage .
    ```

-   or use pre-built image

    ```bash
    docker pull luigi311/anchorleverage:latest
    ```

#### With variables

-   Run

    ```bash
    docker run --rm -it -e MNEMONIC='Seed Phrase Here' luigi311/anchorleverage:latest
    ```

#### With .env

-   Create a .env file similar to .env.sample and set the MNEMONIC variable to your seed phrase

-   Run

    ```bash
     docker run --rm -it -v "$(pwd)/.env:/app/.env" luigi311/anchorleverage:latest
    ```

-   Password will be required to decrypt or encrypt your mnemonic which can be supplied via stdin so it isnt in your history or the PASSWORD variable if stdin is not an option

## Usage

Be sure to edit the .env file to set the variables you need with values you want to use. The .env.sample file is a good starting point and contains descriptions for what exactly each variable is for.

## Contributing

I am open to recieving pull requests. If you are submitting a pull request, please make sure run it locally for a day or two to make sure it is working as expected and stable. Make all pull requests against the dev branch and nothing will be merged into the main without going through the lower branches.

Everything must be ran through with black via

```bash
    black  .
```

## License

This is currently under the MIT license.
