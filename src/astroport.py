import base64

from terra_sdk.core.coins import Coins
from terra_sdk.core.coins import Coin

from src.functions import logger


def swap_token_astroport(
    terra_instance,
    amount,
    from_token_addr,
    to_token_addr,
    contract_pair_addr,
    belief_price,
):
    try:
        denominator = 10 ** 6

        token_from_native = terra_instance.check_native(from_token_addr)

        contract_address = ""
        coins = Coins()
        msg = {}

        if token_from_native:
            contract_address = contract_pair_addr
            coin = Coin(from_token_addr, int(amount * denominator)).to_data()
            coins = Coins.from_data([coin])
            msg = {
                "swap": {
                    "max_spread": str(terra_instance.max_spread),
                    "belief_price": str(belief_price),
                    "offer_asset": {
                        "info": {"native_token": {"denom": str(from_token_addr)}},
                        "amount": str(int(amount * denominator)),
                    },
                }
            }
        else:
            denominator = terra_instance.get_token_denominator(from_token_addr)
            contract_address = from_token_addr
            msg = {
                "send": {
                    "msg": base64.b64encode(
                        str.encode(
                            '{"swap":{"max_spread":"'
                            + str(terra_instance.max_spread)
                            + '","belief_price":"'
                            + str(belief_price)
                            + '"}}'
                        )
                    ).decode("utf-8"),
                    "amount": str(int(amount * denominator)),
                    "contract": str(contract_pair_addr),
                }
            }

        output = terra_instance.contract_executor(contract_address, msg, coins)
        logger(f"Astroport TXHASH: {output}", log_type=0)

    except:
        raise Exception(
            f"Error swappping {from_token_addr} to {to_token_addr} via {contract_pair_addr} on astroport"
        )


def get_belief_price_astroport(
    terra_instance, amount, from_token_addr, to_token_addr, contract_pair_addr
):
    try:
        token_from_native = terra_instance.check_native(from_token_addr)

        denominator = 10 ** 6

        if token_from_native:
            msg = {
                "simulation": {
                    "offer_asset": {
                        "amount": str(int(amount * denominator)),
                        "info": {"native_token": {"denom": str(from_token_addr)}},
                    }
                }
            }
        else:
            denominator = terra_instance.get_token_denominator(from_token_addr)
            msg = {
                "simulation": {
                    "offer_asset": {
                        "amount": str(int(amount * denominator)),
                        "info": {"token": {"contract_addr": str(from_token_addr)}},
                    }
                }
            }

        query_output = terra_instance.contract_query(contract_pair_addr, msg)

        output = (amount * denominator) / int(query_output["return_amount"])

        return output
    except:
        raise Exception(
            f"Error getting belief price for {from_token_addr} to {to_token_addr} via {contract_pair_addr} on astroport"
        )
