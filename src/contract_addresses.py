def terra_contract_addresses(network):
    if network == "MAINNET":
        contracts = {
            "aUST_token": "terra1hzh9vpxhsk8253se0vv5jj6etdvxu3nv8z07zu",
            "bLUNA_token": "terra1kc87mu460fwkqte29rquh4hc20m54fxwtsx7gp",
            "anc_token": "terra14z56l0fp2lsf86zy3hty2z47ezkhnthtr9yq76",
            "mmOracle": "terra1cgg6yef7qcdm070qftghfulaxmllgmvk77nc7t",
            "mmMarket": "terra1sepfj7s0aeg5967uxnfk4thzlerrsktkpelm5s",
            "mmOverseer": "terra1tmnqgvg567ypvsvk6rwsga3srp7e3lg6u0elp8",
            "anchor_bluna_custody": "terra1ptjp2vfjrwh0j0faj9r6katm640kgjxnwwq9kn",
            "ust_luna_contract_terraswap": "terra1tndcaqxkpc5ce9qee5ggqf430mr2z3pefe5wj6",
            "ust_luna_contract_astroport": "terra1m6ywlgn6wrjuagcmmezzz2a029gtldhey5k552",
            "luna_bluna_contract_terraswap": "terra1jxazgm67et0ce260kvrpfv50acuushpjsz2y0p",
            "luna_bluna_contract_astroport": "terra1j66jatn3k50hjtg2xemnjm8s7y8dws9xqa5y8w",
            "anc_ust_contract_terraswap": "terra1gm5p3ner9x9xpwugn9sp6gvhd0lwrtkyrecdn3",
            "anc_ust_contract_astroport": "terra1qr2k6yjjd5p2kaewqvg93ag74k6gyjr7re37fs",
        }
    else:
        contracts = {
            "aUST_token": "terra1ajt556dpzvjwl0kl5tzku3fc3p3knkg9mkv8jl",
            "bLUNA_token": "terra1u0t35drzyy0mujj8rkdyzhe264uls4ug3wdp3x",
            "anc_token": "terra1747mad58h0w4y589y3sk84r5efqdev9q4r02pc",
            "mmOracle": "terra1p4gg3p2ue6qy2qfuxtrmgv2ec3f4jmgqtazum8",
            "mmMarket": "terra15dwd5mj8v59wpj0wvt233mf5efdff808c5tkal",
            "mmOverseer": "terra1qljxd0y3j3gk97025qvl3lgq8ygup4gsksvaxv",
            "anchor_bluna_custody": "terra1ltnkx0mv7lf2rca9f8w740ashu93ujughy4s7p",
            "ust_luna_contract_terraswap": "terra156v8s539wtz0sjpn8y8a8lfg8fhmwa7fy22aff",
            "ust_luna_contract_astroport": "terra12eq2zmdmycvx9n6skwpu9kqxts0787rekjnlwm",
            "luna_bluna_contract_terraswap": "terra13e4jmcjnwrauvl2fnjdwex0exuzd8zrh5xk29v",
            "luna_bluna_contract_astroport": "terra13e4jmcjnwrauvl2fnjdwex0exuzd8zrh5xk29v",
            "anc_ust_contract_terraswap": "terra1wfvczps2865j0awnurk9m04u7wdmd6qv3fdnvz",
            "anc_ust_contract_astroport": "terra13r3vngakfw457dwhw9ef36mc8w6agggefe70d9",
        }
    return contracts
