import requests, os, base64, pwinput

from dotenv import load_dotenv
from time import sleep

# Terra functions
from terra_sdk.client.lcd import LCDClient
from terra_sdk.key.mnemonic import MnemonicKey
from terra_sdk.core.wasm import MsgExecuteContract
from terra_sdk.core.broadcast import is_tx_error

# Encryption functions
from cryptography.exceptions import InvalidSignature
from cryptography.fernet import Fernet, InvalidToken
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

# Custom functions
from src.functions import logger, str_to_bool, requests_retry_session
from src.contract_addresses import terra_contract_addresses
from src.swap import get_leverage_swapping_ratio

load_dotenv(override=True)

public_lcd_env = os.getenv("PUBLIC_LCD_URL")
public_fcd_env = os.getenv("PUBLIC_FCD_URL")
mantle_url_env = os.getenv("TERRA_MANTLE_URL")
chain_id = os.getenv("CHAIN_ID", "bombay-12")
timeout_duration = float(os.getenv("TIMEOUT_DURATION", "30"))
retries = 5

native_denominator = 10 ** 6

# Default to testnet
network = "TESTNET"
public_lcd_url = "https://bombay-lcd.terra.dev"
public_fcd_url = "https://bombay-fcd.terra.dev"
mantle_url = "https://bombay-mantle.terra.dev"
terraswap_api = "https://api-bombay.terraswap.io"

# If chain_id is set to columbus-5 set network to mainnet and set the correct LCD and FCD URLs
if chain_id in ["columbus-5"]:
    network = "MAINNET"
    public_lcd_url = "https://lcd.terra.dev"
    public_fcd_url = "https://fcd.terra.dev"
    mantle_url = "https://mantle.terra.dev"
    terraswap_api = "https://api.terraswap.io"

# If the .env has fcd/lcd URLs set, use those instead
if public_fcd_env is not None:
    public_fcd_url = public_fcd_env

if public_lcd_env is not None:
    public_lcd_url = public_lcd_env

if mantle_url_env is not None:
    mantle_url = mantle_url_env


def get_terra_gas_prices():
    try:
        r = requests_retry_session().get(
            f"{public_fcd_url}/v1/txs/gas_prices", timeout=timeout_duration
        )
        r.raise_for_status()
        if r.status_code == 200:
            return r.json()
    except requests.exceptions.HTTPError as err:
        logger(
            f"Could not fetch get_terra_gas_prices from Terra's FCD. Error message: {err}",
            log_type=2,
        )


def encrypt_mnemonic(mnemonic, password, salt=None):
    try:
        logger("Encrypting mnemonic...", log_type=0)
        if salt is None or salt.strip() == "":
            salt = os.urandom(16)

        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=salt,
            iterations=390000,
        )

        key = base64.urlsafe_b64encode(kdf.derive(password.encode("utf-8")))
        f = Fernet(key)

        encrypted_mnemonic = f.encrypt(mnemonic.encode("utf-8"))

        # Add your mnemonic in .env
        with open(".env", "r") as f:
            lines = f.readlines()
        with open(".env", "w") as f:
            for line in lines:
                if line.startswith("MNEMONIC"):
                    line = f"MNEMONIC = {encrypted_mnemonic}\n"
                if line.startswith("SALT"):
                    line = f"SALT = {salt}\n"
                f.write(line)

        return encrypted_mnemonic, salt
    except Exception as e:
        logger(f"Could not encrypt mnemonic. Error message: {e}", log_type=2)


def decrypt_mnemonic(mnemonic, salt, password):
    try:
        if mnemonic is None:
            raise Exception("Could not decrypt mnemonic. MNEMONIC not found in .env")

        if not isinstance(mnemonic, bytes):
            # Parse the salt from string b'<salt>' to bytes b'<salt>' without adding escape characters
            mnemonic = (
                mnemonic[2:-1]
                .encode()
                .decode("unicode_escape")
                .encode("raw_unicode_escape")
            )

        if salt is not None or salt.strip() != "":
            if not isinstance(salt, bytes):
                # Parse the salt from string b'<salt>' to bytes b'<salt>' without adding escape characters
                salt = (
                    salt[2:-1]
                    .encode()
                    .decode("unicode_escape")
                    .encode("raw_unicode_escape")
                )
        else:
            salt = None

        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=salt,
            iterations=390000,
        )

        key = base64.urlsafe_b64encode(kdf.derive(password.encode("utf-8")))

        f = Fernet(key)
        decrypted_mnemonic = f.decrypt(mnemonic).decode("utf-8")

        return decrypted_mnemonic
    except InvalidSignature:
        raise Exception("Could not decrypt mnemonic. Password is incorrect.")
    except InvalidToken:
        raise Exception("Could not decrypt mnemonic. Password is incorrect.")
    except Exception as e:
        raise Exception(f"Could not decrypt mnemonic. Error message: {e}")


def parse_mnemonic():
    try:
        mnemonic = os.getenv("MNEMONIC", None)
        salt = os.getenv("SALT", None)
        password = os.getenv("PASSWORD", None)

        encrypt = False

        # If mnemonic is not set in .env grab it from the user
        if mnemonic is None or mnemonic.strip() == "":
            mnemonic = input("Enter your mnemonic: ").strip()

        if password is None:
            password = pwinput.pwinput("Enter your password: ")

        # If mnemonic is not enccrypted, encrypt it
        if not mnemonic.startswith("b'"):
            encrypt = True

        if encrypt:
            mnemonic, salt = encrypt_mnemonic(mnemonic, password, salt)

        mnemonic = decrypt_mnemonic(mnemonic, salt, password)

        return mnemonic

    except Exception as e:
        raise Exception(f"{e}")


class Terra:
    def __init__(self):
        """Terra class containing all common functions for interacting with Terra."""
        logger("Initializing Terra class", log_type=1)
        logger(f"Using {network} network", log_type=0)
        self.chain_id = chain_id
        self.public_node_url = public_lcd_url
        self.mantle_url = mantle_url
        self.terraswap_api = terraswap_api
        self.tx_look_up = f"https://finder.terra.money/{self.chain_id}/tx/"
        self.get_contract_addresses = terra_contract_addresses(network)

        # Contract required
        self.aUST_token = self.get_contract_addresses["aUST_token"]
        self.bLUNA_token = self.get_contract_addresses["bLUNA_token"]
        self.anc_token = self.get_contract_addresses["anc_token"]
        self.mmMarket = self.get_contract_addresses["mmMarket"]
        self.mmOverseer = self.get_contract_addresses["mmOverseer"]
        self.mmOracle = self.get_contract_addresses["mmOracle"]
        self.anchor_bluna_custody = self.get_contract_addresses["anchor_bluna_custody"]
        self.ust_luna_contract_terraswap = self.get_contract_addresses[
            "ust_luna_contract_terraswap"
        ]
        self.ust_luna_contract_astroport = self.get_contract_addresses[
            "ust_luna_contract_astroport"
        ]
        self.luna_bluna_contract_terraswap = self.get_contract_addresses[
            "luna_bluna_contract_terraswap"
        ]
        self.luna_bluna_contract_astroport = self.get_contract_addresses[
            "luna_bluna_contract_astroport"
        ]
        self.anc_ust_contract_terraswap = self.get_contract_addresses[
            "anc_ust_contract_terraswap"
        ]
        self.anc_ust_contract_astroport = self.get_contract_addresses[
            "anc_ust_contract_astroport"
        ]

        # Load configuration variables
        self.notify_telegram = str_to_bool(os.getenv("NOTIFY_TELEGRAM", "False"))
        self.sleep_duration = float(os.getenv("SLEEP_DURATION", "600"))
        self.timeout = timeout_duration
        self.retries = retries
        self.wallet_min_ust_balance = float(os.getenv("WALLET_MIN_UST_BALANCE", "10"))
        self.wallet_max_ust_balance = float(os.getenv("WALLET_MAX_UST_BALANCE", "20"))
        self.anchor_usage_min_percentage = (
            float(os.getenv("ANCHOR_USAGE_MIN_PERCENTAGE", "60")) / 100
        )
        self.anchor_usage_max_percentage = (
            float(os.getenv("ANCHOR_USAGE_MAX_PERCENTAGE", "75")) / 100
        )
        self.anchor_usage_target_percentage = (
            float(os.getenv("ANCHOR_USAGE_TARGET_PERCENTAGE", "65")) / 100
        )
        self.anchor_anc_max_balance = float(os.getenv("ANCHOR_ANC_MAX_BALANCE", "20"))
        self.ust_earn_to_borrow_target_ratio = (
            float(os.getenv("UST_EARN_TO_BORROW_TARGET_RATIO", "80")) / 100
        )
        self.ust_earn_to_borrow_min_ratio = (
            float(os.getenv("UST_EARN_TO_BORROW_MIN_RATIO", "10")) / 100
        )
        self.ust_earn_to_borrow_max_ratio = (
            float(os.getenv("UST_EARN_TO_BORROW_MAX_RATIO", "100")) / 100
        )
        self.deleverage_borrow_usage_target = (
            float(os.getenv("DELEVERAGE_BORROW_USAGE_TARGET", "90")) / 100
        )
        self.max_spread = float(os.getenv("MAX_SPREAD", "0.01"))

        # Load Terra LCD client
        self.terra = LCDClient(
            chain_id=self.chain_id,
            url=self.public_node_url,
            gas_prices=get_terra_gas_prices(),
            gas_adjustment=2,
        )

        mnemonic = parse_mnemonic()

        self.check_errors(mnemonic)
        # Load wallet

        self.mk = MnemonicKey(mnemonic=mnemonic)
        self.wallet = self.terra.wallet(self.mk)

        self.leverage_swapping_ratio = get_leverage_swapping_ratio(self)

        logger(
            f"Terra client initialized, wallet {self.wallet.key.acc_address}",
            log_type=1,
        )

    def check_errors(self, mnemonic=None):
        if mnemonic is None or mnemonic == "":
            raise Exception("MNEMONIC is not set")

        if (
            self.anchor_usage_max_percentage < self.anchor_usage_target_percentage
            or self.anchor_usage_max_percentage < self.anchor_usage_min_percentage
        ):
            raise Exception(
                f"ANCHOR_USAGE_MAX_PERCENTAGE ({self.anchor_usage_max_percentage * 100}) must be greater than ANCHOR_USAGE_MIN_PERCENTAGE ({self.anchor_usage_min_percentage * 100}) and ANCHOR_USAGE_TARGET_PERCENTAGE ({self.anchor_usage_target_percentage * 100})"
            )

        if (
            self.anchor_usage_min_percentage > self.anchor_usage_target_percentage
            or self.anchor_usage_min_percentage > self.anchor_usage_max_percentage
        ):
            raise Exception(
                f"ANCHOR_USAGE_MIN_PERCENTAGE ({self.anchor_usage_min_percentage * 100}) must be less than ANCHOR_USAGE_MAX_PERCENTAGE ({self.anchor_usage_max_percentage * 100}) and ANCHOR_USAGE_TARGET_PERCENTAGE ({self.anchor_usage_target_percentage * 100})"
            )

        if self.ust_earn_to_borrow_target_ratio < 0:
            raise Exception(
                f"UST_EARN_TO_BORROW_TARGET_RATIO {self.ust_earn_to_borrow_target_ratio} must be greater than 0"
            )

        if (
            self.ust_earn_to_borrow_min_ratio > self.ust_earn_to_borrow_target_ratio
            or self.ust_earn_to_borrow_min_ratio > self.ust_earn_to_borrow_max_ratio
        ):
            raise Exception(
                f"UST_EARN_TO_BORROW_MIN_RATIO {self.ust_earn_to_borrow_min_ratio} must be less than UST_EARN_TO_BORROW_TARGET_RATIO {self.ust_earn_to_borrow_target_ratio} and less than UST_EARN_TO_BORROW_MAX_RATIO {self.ust_earn_to_borrow_max_ratio}"
            )

        if (
            self.ust_earn_to_borrow_max_ratio < self.ust_earn_to_borrow_min_ratio
            or self.ust_earn_to_borrow_max_ratio < self.ust_earn_to_borrow_target_ratio
        ):
            raise Exception(
                f"UST_EARN_TO_BORROW_MAX_RATIO {self.ust_earn_to_borrow_max_ratio} must be greater than UST_EARN_TO_BORROW_MIN_RATIO {self.ust_earn_to_borrow_min_ratio} and greater than UST_EARN_TO_BORROW_TARGET_RATIO {self.ust_earn_to_borrow_target_ratio}"
            )

        if (
            self.deleverage_borrow_usage_target < self.anchor_usage_min_percentage
            or self.deleverage_borrow_usage_target < self.anchor_usage_target_percentage
        ):
            raise Exception(
                f"DELEVERAGE_BORROW_USAGE_TARGET {self.deleverage_borrow_usage_target} must be greater than ANCHOR_USAGE_MIN_PERCENTAGE {self.anchor_usage_min_percentage} and greater than ANCHOR_USAGE_TARGET_PERCENTAGE {self.anchor_usage_target_percentage}.\nIt is recommended to place it as high as possible to allow for the fastest possible deleverage so you can return to your target amount"
            )

    def get_block_height(self):
        for attempt in range(0, self.retries):
            try:
                return self.terra.tendermint.block_info()["block"]["header"]["height"]

            except Exception as err:
                sleep_timer = 2 * attempt

                msg = f"Getting block height failed\n{err}"
                logger(f"{msg}\n retrying in {sleep_timer} seconds", log_type=1)

                if attempt == (self.retries - 1):
                    raise Exception(msg)

                sleep(sleep_timer)

    def get_account_native_balance(self):
        for attempt in range(0, self.retries):
            try:
                balances = self.terra.bank.balance(
                    self.wallet.key.acc_address
                ).to_data()

                output = {}
                for balance in balances:
                    symbol = balance["denom"][1:]

                    output[symbol] = float(balance["amount"]) / native_denominator

                return output

            except Exception as err:
                sleep_timer = 2 * attempt

                msg = f"Getting native balances failed\n{err}"
                logger(f"{msg}\n retrying in {sleep_timer} seconds", log_type=1)

                if attempt == (self.retries - 1):
                    raise Exception(msg)

                sleep(sleep_timer)

    def check_native(self, token_address):
        for attempt in range(0, self.retries):
            try:
                # If token_address does not start with u then it will prepend it as all natives start with u
                if token_address[0] != "u":
                    token_address = "u" + token_address

                if token_address == "uluna":
                    return True

                rates = self.terra.oracle.exchange_rates().to_data()
                for rate in rates:
                    if token_address in rate["denom"]:
                        return True

                return False

            except Exception as err:
                sleep_timer = 2 * attempt

                msg = f"Getting native exchange rates failed\n{err}"
                logger(f"{msg}\n retrying in {sleep_timer} seconds", log_type=1)

                if attempt == (self.retries - 1):
                    raise Exception(msg)

                sleep(sleep_timer)

    def check_tx_info(self, tx_hash: str):
        for attempt in range(0, self.retries):
            try:
                tx_info = self.terra.tx.tx_info(tx_hash)
                sleep(1)

                return tx_info

            except Exception as err:
                sleep_timer = 2 * attempt

                if attempt == (self.retries - 1):
                    raise Exception(err)

                sleep(sleep_timer)

    def contract_query(self, contract_addr, execute_msg):
        for attempt in range(0, self.retries):
            try:
                result = self.terra.wasm.contract_query(contract_addr, execute_msg)
                return result
            except Exception as err:
                sleep_timer = 2 * attempt

                msg = f"Contract query failed for {contract_addr}\n{execute_msg}\n{err}"
                logger(f"{msg}\n retrying in {sleep_timer} seconds", log_type=1)

                if attempt == (self.retries - 1):
                    raise Exception(msg)

                sleep(sleep_timer)

    def contract_executor(self, contract_addr, execute_msg, send_coins):
        execute = MsgExecuteContract(
            sender=self.wallet.key.acc_address,
            contract=contract_addr,
            execute_msg=execute_msg,
            coins=send_coins,
        )

        logger(execute, log_type=1)
        for attempt in range(0, self.retries):
            try:
                sendtx = self.wallet.create_and_sign_tx(
                    msgs=[execute],
                    gas_adjustment="1.2",
                    fee_denoms=["uusd"],
                )
                result = self.terra.tx.broadcast(sendtx)

                error = is_tx_error(result)
                if error:
                    raise Exception(
                        f"Transaction failed, TX_ID: {result.txhash}\nError: {result.raw_log}"
                    )

                # Takes about 7 seconds for block to process, set to 30 seconds to avoid issues when swapping luna not being in the wallet
                sleep(30)
                return result.txhash

            except Exception as err:
                sleep_timer = 2 * attempt

                logger(f"{err}\n retrying in {sleep_timer} seconds", log_type=1)

                if attempt == (self.retries - 1):
                    raise Exception(err)

                sleep(sleep_timer)

    def get_token_denominator(self, token_address):
        denominator_url = (
            self.public_node_url + "/wasm/contracts/" + token_address + "/store"
        )
        denominator_query_msg = '{"token_info": {}}'

        # Get "decimals" from response no matter how deep it is
        for attempt in range(0, self.retries):
            try:
                response = (
                    requests_retry_session()
                    .get(
                        url=denominator_url,
                        params={"query_msg": denominator_query_msg},
                        timeout=timeout_duration,
                    )
                    .json()
                )
                denominator_power = float(response["result"]["decimals"])
                denominator = 10 ** denominator_power
                return denominator

            except Exception as err:
                sleep_timer = 2 * attempt

                msg = f"Could not get denominator for token {token_address}\nURL: {denominator_url}\nQuery_msg: {denominator_query_msg}\nError: {err}"
                logger(f"{msg}\n retrying in {sleep_timer} seconds", log_type=1)

                if attempt == (self.retries - 1):
                    raise Exception(msg)

                sleep(sleep_timer)

    def get_token_amount(self, token_address):
        for attempt in range(0, self.retries):
            try:
                logger(f"Getting token amount for {token_address}", log_type=1)

                query_msg = {"balance": {"address": self.wallet.key.acc_address}}
                token_amount = self.contract_query(token_address, query_msg)

                denominator = self.get_token_denominator(token_address)

                output = int(token_amount["balance"]) / denominator

                logger(f"Token balance {token_address}: {output}", log_type=1)
                return output
            except Exception as err:
                sleep_timer = 2 * attempt

                msg = f"Failed to get token amount for {token_address}\n{err}"
                logger(f"{msg}\n retrying in {sleep_timer} seconds", log_type=1)

                if attempt == (self.retries - 1):
                    raise Exception(msg)

                sleep(sleep_timer)
