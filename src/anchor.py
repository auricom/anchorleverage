import json
from time import sleep

from terra_sdk.core.coins import Coins
from terra_sdk.core.coins import Coin

from src.functions import logger, requests_retry_session
from src.terraswap import get_belief_price_terraswap
from src.astroport import get_belief_price_astroport

native_denominator = 10 ** 6


def get_anc_pending_amount(terra_instance):
    msg = {
        "borrower_info": {
            "borrower": terra_instance.wallet.key.acc_address,
        }
    }
    anc_amount = float(
        terra_instance.contract_query(terra_instance.mmMarket, msg)["pending_rewards"]
    )
    denominator = terra_instance.get_token_denominator(terra_instance.anc_token)

    output = anc_amount / denominator
    return output


def get_anc_pending_value(terra_instance):
    amount = get_anc_pending_amount(terra_instance)

    if amount <= 0:
        return 0

    price_terraswap = get_belief_price_terraswap(
        terra_instance,
        amount,
        terra_instance.anc_token,
        "uusd",
        terra_instance.anc_ust_contract_terraswap,
    )
    price_astroport = get_belief_price_astroport(
        terra_instance,
        amount,
        terra_instance.anc_token,
        "uusd",
        terra_instance.anc_ust_contract_astroport,
    )

    if price_terraswap < price_astroport:
        price = price_terraswap
    else:
        price = price_astroport

    output = amount / price

    logger(f"Pending ANC Value: {output}", log_type=1)

    return output


def get_anc_value(terra_instance):
    amount = terra_instance.get_token_amount(terra_instance.anc_token)

    if amount <= 0:
        return 0

    price_terraswap = get_belief_price_terraswap(
        terra_instance,
        amount,
        terra_instance.anc_token,
        "uusd",
        terra_instance.anc_ust_contract_terraswap,
    )
    price_astroport = get_belief_price_astroport(
        terra_instance,
        amount,
        terra_instance.anc_token,
        "uusd",
        terra_instance.anc_ust_contract_astroport,
    )

    if price_terraswap < price_astroport:
        price = price_terraswap
    else:
        price = price_astroport

    output = amount / price

    logger(f"Wallet ANC Value: {output}", log_type=1)

    return output


def get_aust_value(terra_instance):
    output = terra_instance.get_token_amount(terra_instance.aUST_token) * get_aUST_rate(
        terra_instance
    )

    return output


def get_aUST_rate(terra_instance):
    query_msg_aUST_rate = {
        "epoch_state": {},
    }

    aUST_rate_result = terra_instance.contract_query(
        terra_instance.mmMarket, query_msg_aUST_rate
    )

    output = float(aUST_rate_result["exchange_rate"])

    return output


def get_anchor_oracle_prices(terra_instance):
    # mantle will sometimes return no data but will return a status of 200 so it needs to be retried instead of just failing
    for attempt in range(0, terra_instance.retries):
        try:
            url = terra_instance.mantle_url + "/?borrow--market"
            query_msg_oracle_prices = (
                '{\n  oraclePrices: WasmContractsContractAddressStore(\n    ContractAddress: "'
                + terra_instance.mmOracle
                + '"\n    QueryMsg: "{\\"prices\\":{}}"\n  ) {\n    Result\n    }\n }\n'
            )

            response = requests_retry_session().post(
                url,
                json={"query": query_msg_oracle_prices},
                timeout=terra_instance.timeout,
            )

            result = json.loads(response.json()["data"]["oraclePrices"]["Result"])[
                "prices"
            ]

            output = {}
            for asset in result:
                output[asset["asset"]] = float(asset["price"])

            return output
        except Exception as e:
            sleep_timer = 2 * attempt
            msg = f"Failed to get oracle prices,\n{url}\n{query_msg_oracle_prices}\n Status Code: {response.status_code}\n{e}"

            logger(f"{msg}\n retrying in {sleep_timer} seconds", log_type=1)

            if attempt == (terra_instance.retries - 1):
                raise Exception(msg)

            sleep(sleep_timer)


def get_anchor_borrow_amount(terra_instance):
    # https://github.com/Anchor-Protocol/money-market-contracts/blob/main/contracts/market/src/borrow.rs#L376
    query_msg_loan = {
        "borrower_info": {
            "borrower": terra_instance.wallet.key.acc_address,
            "block_height": 1,
        },
    }

    loan_amount_result = terra_instance.contract_query(
        terra_instance.mmMarket, query_msg_loan
    )

    output = int(loan_amount_result["loan_amount"]) / native_denominator

    logger(f"Borrow Amount: {output}", log_type=1)

    return output


def get_anchor_borrow_limit(terra_instance):
    # Get loan borrow limit
    query_msg_borrow_limit = {
        "borrow_limit": {
            "borrower": terra_instance.wallet.key.acc_address,
        },
    }

    borrow_limit_result = terra_instance.contract_query(
        terra_instance.mmOverseer, query_msg_borrow_limit
    )

    output = int(borrow_limit_result["borrow_limit"]) / native_denominator

    return output


def get_anchor_collateral_amounts(terra_instance):
    output = {}

    query_msg_collateral = {
        "collaterals": {
            "borrower": terra_instance.wallet.key.acc_address,
        }
    }

    collateral_amount_result = terra_instance.contract_query(
        terra_instance.mmOverseer, query_msg_collateral
    )["collaterals"]

    for collateral in collateral_amount_result:
        token_address = collateral[0]
        amount = collateral[1]

        denominator = terra_instance.get_token_denominator(token_address)
        output[token_address] = int(amount) / native_denominator

    return output


def get_anchor_collateral_value(terra_instance, asset, amount):
    anchor_prices = get_anchor_oracle_prices(terra_instance)

    if asset not in anchor_prices:
        raise Exception(f"{asset} not in anchor_prices")

    output = amount * anchor_prices[asset]

    return output


def get_anchor_all_collateral_value(terra_instance):
    # Get anchor total collateral value
    anchor_prices = get_anchor_oracle_prices(terra_instance)

    collateral_amount_result = get_anchor_collateral_amounts(terra_instance)

    output = 0
    for key, value in collateral_amount_result.items():
        token_price = anchor_prices[key]

        output += value * token_price

    logger(f"Collateral Value: {output}", log_type=1)

    return output


def get_anchor_borrow_usage(terra_instance):
    # Calculate anchor borrow usage
    borrowed_amount = get_anchor_borrow_amount(terra_instance)
    borrow_limit = get_anchor_borrow_limit(terra_instance)

    if borrow_limit > 0:
        output = borrowed_amount / borrow_limit
    else:
        output = 0

    logger(f"Borrow Usage: {output}", log_type=1)

    return output


def get_anchor_asset_borrow_limit(terra_instance, asset):
    # Get loan borrow limit
    query_msg_borrow_limit = {"whitelist": {}}

    borrow_limit_result = terra_instance.contract_query(
        terra_instance.mmOverseer, query_msg_borrow_limit
    )

    for i in borrow_limit_result["elems"]:
        if i["collateral_token"] == asset:
            output = float(i["max_ltv"])

    return output


def get_earn_to_borrow_ratio(terra_instance):
    earn_borrow_ratio = None

    borrowed_amount = get_anchor_borrow_amount(terra_instance)
    earn_amount = get_aust_value(terra_instance)

    if borrowed_amount != 0 and earn_amount != 0:
        earn_borrow_ratio = earn_amount / borrowed_amount

    return earn_borrow_ratio


def anchor_borrow_repay(terra_instance, ust_amount):
    # Repay anchor borrow loan
    ust_amount = int(ust_amount * native_denominator)
    coins = Coins.from_str(f"{ust_amount}uusd")
    contract_address = terra_instance.mmMarket
    msg = {"repay_stable": {}}

    output = terra_instance.contract_executor(contract_address, msg, coins)

    logger(f"Anchor Borrow Repay TXHASH: {output}", log_type=0)

    return output


def anchor_borrow_borrow(terra_instance, ust_amount):
    # Borrow additional UST from anchor borrow
    ust_amount = int(ust_amount * native_denominator)
    coins = Coins()
    contract_address = terra_instance.mmMarket
    msg = {"borrow_stable": {"borrow_amount": f"{ust_amount}"}}

    output = terra_instance.contract_executor(contract_address, msg, coins)

    logger(f"Anchor Borrow Borrow TXHASH: {output}", log_type=0)

    return output


def anchor_deposit_bluna_collateral(terra_instance, bluna_amount):
    # Deposit bluna collateral to anchor borrow
    logger(f"Depositing {bluna_amount} bluna amount to anchor borrow", log_type=0)
    amount = int(bluna_amount * native_denominator)

    coins = Coins()
    contract_address = terra_instance.bLUNA_token
    msg = {
        "send": {
            "msg": "eyJkZXBvc2l0X2NvbGxhdGVyYWwiOnt9fQ==",
            "amount": f"{amount}",
            "contract": f"{terra_instance.anchor_bluna_custody}",
        }
    }

    tx_hash = terra_instance.contract_executor(contract_address, msg, coins)

    logger(f"Despoited bluna to Anchor bLuna Custody TXHASH: {tx_hash}", log_type=0)

    logger("Locking bluna to overseer", log_type=0)
    contract_address = terra_instance.mmOverseer
    msg = {
        "lock_collateral": {
            "collaterals": [[f"{terra_instance.bLUNA_token}", f"{amount}"]]
        }
    }
    tx_hash = terra_instance.contract_executor(contract_address, msg, coins)

    logger(f"Locked bluna to overseer TXHASH: {tx_hash}", log_type=0)


def anchor_withdrawal_bluna_collateral(terra_instance, bluna_amount):
    # Withdrawing bluna collateral to anchor borrow
    logger(f"Withdrawing {bluna_amount} bluna amount from anchor borrow", log_type=0)
    amount = int(bluna_amount * native_denominator)

    coins = Coins()
    contract_address = terra_instance.mmOverseer
    msg = {
        "unlock_collateral": {
            "collaterals": [[f"{terra_instance.bLUNA_token}", f"{amount}"]]
        }
    }

    tx_hash = terra_instance.contract_executor(contract_address, msg, coins)

    logger(f"Unlocked bluna from Overseer TXHASH: {tx_hash}", log_type=0)

    logger("withdrawing bluna", log_type=0)
    contract_address = terra_instance.anchor_bluna_custody
    msg = {"withdraw_collateral": {"amount": f"{amount}"}}
    tx_hash = terra_instance.contract_executor(contract_address, msg, coins)

    logger(f"Withdrawed bluna TXHASH: {tx_hash}", log_type=0)


def anchor_earn_withdraw(terra_instance, ust_amount):
    ust_amount = ust_amount * native_denominator
    aUST_rate = get_aUST_rate(terra_instance)
    withdrawal_amount = int(ust_amount / aUST_rate)

    coins = Coins()
    contract_address = terra_instance.aUST_token
    msg = {
        "send": {
            "msg": "eyJyZWRlZW1fc3RhYmxlIjp7fX0=",
            "amount": f"{withdrawal_amount}",
            "contract": f"{terra_instance.mmMarket}",
        }
    }

    output = terra_instance.contract_executor(contract_address, msg, coins)

    return output


def anchor_earn_deposit(terra_instance, ust_amount):
    ust_amount = int(ust_amount * native_denominator)
    coin = Coin("uusd", ust_amount).to_data()
    coins = Coins.from_data([coin])

    contract_address = terra_instance.mmMarket
    msg = {"deposit_stable": {}}

    output = terra_instance.contract_executor(contract_address, msg, coins)

    return output


def anchor_claim_anc_rewards(terra_instance):
    coins = Coins()
    msg = {"claim_rewards": {}}
    contract = terra_instance.mmMarket

    output = terra_instance.contract_executor(contract, msg, coins)

    return output
