import os, requests, json
from dotenv import load_dotenv

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

load_dotenv(override=True)

telegram_chat_id = os.getenv("TELEGRAM_CHAT_ID")
token = os.getenv("TELEGRAM_TOKEN")

if telegram_chat_id is not None and telegram_chat_id.strip() != "":
    telegram_chat_id = int(telegram_chat_id)

# Retry requests from https://www.peterbe.com/plog/best-practice-with-retries-with-requests
def requests_retry_session(
    retries=5,
    backoff_factor=0.3,
    status_forcelist=(500, 502, 504),
    session=None,
):
    session = session or requests.Session()

    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)

    session.mount("http://", adapter)
    session.mount("https://", adapter)

    return session


def telegram_notification(msg):
    tg_data = {
        "chat_id": str(telegram_chat_id),
        "text": str(msg).strip().replace("_", ""),
        "parse_mode": "Markdown",
    }

    try:
        response = requests_retry_session().post(
            "https://api.telegram.org/bot" + token + "/sendMessage",
            data=json.dumps(tg_data),
            headers={"Content-Type": "application/json"},
            timeout=5,
        )

        if response.status_code != 200:
            raise Exception(
                f"Request to telegram returned an error {response.status_code}, the response is:\n{response.text}"
            )
    except Exception as e:
        print(f"Telegram error:\ntg_data: {tg_data}\nError: {e}")


# Function logger with parameters (message, log_type) with log_type = 0 (info), 1 (info) or 2 (error)
def logger(message, log_type=0):
    debug = str_to_bool(os.getenv("DEBUG", "False"))
    telegram_notify = str_to_bool(os.getenv("NOTIFY_TELEGRAM", "False"))

    output = str(message)
    if log_type == 0:
        pass
    elif log_type == 1 and debug:
        output = "[INFO]: " + str(output)
    elif log_type == 2:
        output = "[ERROR]: " + str(output)
    else:
        output = None

    if output is not None:
        print(output)
        if telegram_notify:
            telegram_notification(output)


# Reimplementation of distutils.util.strtobool due to it being deprecated
# Source: https://github.com/PostHog/posthog/blob/01e184c29d2c10c43166f1d40a334abbc3f99d8a/posthog/utils.py#L668
def str_to_bool(value: any) -> bool:
    if not value:
        return False
    return str(value).lower() in ("y", "yes", "t", "true", "on", "1")
